console.log("Hello");

// var firebase = app_fireBase;
let email = "";
let name = "";
var logo = document.getElementById('logo');
const msgScreen = document.getElementById("messages");
const msgForm = document.getElementById("messageForm");
const msgInput = document.getElementById("msg-input");
const msgBtn = document.getElementById("msg-btn");
const emojiBtn = document.getElementById("msg-btn");
const picker = new EmojiButton();
// const userName = document.getElementById("user-name");
const db = firebase.database();
const msgRef = db.ref("com_list"); //save in msgs folder in database

function init(){

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          // User is signed in. Get their name.
          //name = user.displayName;
          email = user.email;
          msgRef.on('child_added', updateMsgs);
          // userName.innerHTML = "Welcome, " + name + "!"; //name要再修正sign in那裡
        }else{
            //redirect to login page
            window.location.replace("signin.html");
        }
      });

    document.getElementById('log-out').addEventListener('click', logOut);
    // msgForm.addEventListener('submit', sendMessage);
    msgBtn.addEventListener('click', sendMessage);
    
}

logo.addEventListener('click', function() {
  window.location.href = "index.html";
});


function logOut(){
    firebase.auth().signOut().then(function() {
        console.log("SIGN OUT");
        window.location.replace("signin.html");
      }).catch(function(error) {

        console.error(error);
      });
}

picker.on('emoji', emoji => {
  document.querySelector('input').value += emoji;
});

emojiBtn.addEventListener('click', () => {
  picker.togglePicker(emojiBtn);
});

const updateMsgs = data =>{
  const {email: userEmail , name, text} = data.val();
  
  var outputText = text;
  
  //load messages
  const msg = `<li class="${email == userEmail ? "msg my": "msg"}"><span class = "msg-span">
    <i class = "name">${name}: </i>${outputText}
    </span>
  </li>`
  msgScreen.innerHTML += msg;
  document.getElementById("chat-window").scrollTop = document.getElementById("chat-window").scrollHeight;
  //auto scroll to bottom
}



function sendMessage(e){
  e.preventDefault();
  const text = msgInput.value;
  console.log(text)

    if(!text.trim()) return alert('Please type your message.'); //no msg submitted
    const msg = {
        email,
        //name,
        text: text
    };

    msgRef.push(msg);
    msgInput.value = "";
}

document.addEventListener('DOMContentLoaded',init);


