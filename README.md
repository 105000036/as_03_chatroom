# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Midterm_Project_10500036

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

# 作品網址：<h4>https://105000036.gitlab.io/as_03_chatroom/

## Website Detail Description
一開始會進入聊天室畫面，請先按右上方SignIn按鈕創建帳號，然後再輸入一次即可進入聊天室，聊天室功能簡易，左上logo及右上chat按鈕皆為返回首頁功用，下方為type something可輸入文字，按下send即可傳送訊息至聊天室，若沒有輸入即傳送回跳出警示訊息，同時send按鈕也帶有emoji功能。


# Components Description : 
index.html
1. [Chat] : 包含左上logo皆為返回頁首功能
2. [Signin] : 連結到登入帳戶資料頁面
3. [Logout] : 登出帳號
4. [Send] : 將Text的訊息傳出，如果沒有文字輸入會跳出 警示要求輸入內容，同時此按鈕帶有emoji功能可以選擇。
6. [Text] : 文字輸入區，也可以加入emoji

signin.html
1. [Logo] : 最上方圖片點擊可以返回頁首功能
2. [Email Address] : 請創建email
3. [Password] : 請輸入6位數字以上的密碼
4. [Sign in with Google] : 請使用google帳戶登入
6. [Sign in with Apple] : 請使用apple帳戶登入
7. [New Account] : 若為新建立的用戶請先輸入email.password後再做登入即可進入chatroom

# Other Functions Description : 
1. [Send] : 傳送按鈕帶有emoji功能
